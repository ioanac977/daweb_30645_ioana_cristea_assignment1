import React, {Component} from 'react';
import {Grid, Cell, List, ListItem, ListItemContent} from 'react-mdl';
import profilPhoto from "../images/profilPhoto3.jpg"
import TextField from "@material-ui/core/TextField";
import {TextareaAutosize} from "@material-ui/core";
import Button from "@material-ui/core/Button";
import ApiServiceEmail from "../service/ApiServiceEmail";
import getTranslation from "../constants/getTranslation";
import {store} from "../constants/store"


class Contact extends Component {

    constructor(props){
        super(props);
        this.state ={
            recipient:'ioanac9@yahoo.com',
            subject:'',
            body:'',
            message: null
        }
        this.sendEmail = this.sendEmail.bind(this);
    }
    sendEmail = (e) => {
        e.preventDefault();

        let email = {recipient: this.state.recipient, subject: this.state.subject, body: this.state.body};

        ApiServiceEmail.addEmail(email)
            .then(res => {
                this.setState({message : 'Email added successfully.'});
                this.props.history.push('/success');
            });

    }
    onChange = (e) =>
        this.setState({ [e.target.name]: e.target.value });

    render() {
        return (
    <div>
    <div className="landing-grid">

            </div>
                <div className="contact-body">
                    <Grid className="contact-grid">
                        <Cell col={6}>
                            <h2>Cristea Ioana</h2>
                            <img
                                src={profilPhoto}
                                alt="avatar"
                                style={{height: '200px'}}
                            />
                            <TextField placeholder="email" fullWidth margin=""  name="recipient" value={this.state.recipient} onChange={this.onChange}/>
                            <TextField placeholder="subject" fullWidth margin="normal" name="subject" value={this.state.subject} onChange={this.onChange}/>

                            <TextareaAutosize style={{marginLeft:"-10px",width:"550px",height:"120px"}} placeholder="" fullWidth margin="normal" name="body" value={this.state.body} onChange={this.onChange}/>
                            <Button style={{marginLeft:"-10px",width:"550px",height:"20px",background:"#3da4ab"}} variant="contained" onClick={this.sendEmail}>Send Email</Button>

                        </Cell>
                        <Cell col={6}>
                            <h2> {getTranslation(
                                store.getState(),
                                'numeContact/nameContact',
                            )}</h2>
                            <hr/>

                            <div className="contact-list">
                                <List >
                                    <ListItem >
                                        <ListItemContent style={{fontSize: '30px'}}>
                                            <i  className="fa fa-phone-square" aria-hidden="true"/>
                                            (+40) 774 619 544
                                        </ListItemContent>
                                    </ListItem>

                                    <ListItem>
                                        <ListItemContent style={{fontSize: '30px'}}>
                                            <i className="fa fa-fax" aria-hidden="true"/>
                                            (+20) 868 224
                                        </ListItemContent>
                                    </ListItem>

                                    <ListItem>
                                        <ListItemContent style={{fontSize: '30px'}}>
                                            <i className="fa fa-envelope" aria-hidden="true"/>
                                            ioanac977@gmail.com
                                        </ListItemContent>
                                    </ListItem>

                                    <ListItem>
                                        <ListItemContent style={{fontSize: '30px'}}>
                                            <i className="fa fa-skype" aria-hidden="true"/>
                                            cioana9
                                        </ListItemContent>
                                    </ListItem>


                                </List>
                            </div>
                        </Cell>
                    </Grid>
                    <footer><br></br><br></br><br></br>
                        Made by <a href ="https://github.com/ioanac977/presentation-resume-template">@IoanaCristea</a>
                    </footer>
                </div>


            </div>

        );
    }
}

export default Contact;
